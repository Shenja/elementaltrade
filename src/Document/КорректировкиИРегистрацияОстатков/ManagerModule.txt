﻿//sza130909-2146 : 

Процедура Печать(ТабДок, Ссылка) Экспорт
	
	Макет = Документы.КорректировкиИРегистрацияОстатков.ПолучитьМакет("Печать");
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	КорректировкиИРегистрацияОстатков.ВидЦен,
	|	КорректировкиИРегистрацияОстатков.Дата,
	|	КорректировкиИРегистрацияОстатков.Комментарий,
	|	КорректировкиИРегистрацияОстатков.ОстатокДенег,
	|	КорректировкиИРегистрацияОстатков.ТовараВКоличестве,
	|	КорректировкиИРегистрацияОстатков.ТовараНаСумму,
	|	КорректировкиИРегистрацияОстатков.Товары.(
	|		Склад,
	|		Номенклатура,
	|		Количество,
	|		Цена,
	|		Сумма
	|	),
	|	КорректировкиИРегистрацияОстатков.Расчеты.(
	|		Клиент,
	|		Сумма,
	|		Комментарий
	|	),
	|	КорректировкиИРегистрацияОстатков.Цены.(
	|		Номенклатура,
	|		ВидЦен,
	|		Цена
	|	),
	|	КорректировкиИРегистрацияОстатков.РасчетыСПоставщиками.(
	|		Поставщик,
	|		Сумма,
	|		Комментарий
	|	),
	|	КорректировкиИРегистрацияОстатков.ОстаткиДенег.(
	|		Ссылка,
	|		НомерСтроки,
	|		ОстатокДенег,
	|		Валюта,
	|		Статья
	|	)
	|ИЗ
	|	Документ.КорректировкиИРегистрацияОстатков КАК КорректировкиИРегистрацияОстатков
	|ГДЕ
	|	КорректировкиИРегистрацияОстатков.Ссылка В(&Ссылка)";
	Запрос.Параметры.Вставить("Ссылка", Ссылка);
	Выборка = Запрос.Выполнить().Выбрать();

	ОбластьЗаголовок = Макет.ПолучитьОбласть("Заголовок");
	Шапка = Макет.ПолучитьОбласть("Шапка");
	ОбластьТоварыШапка = Макет.ПолучитьОбласть("ТоварыШапка");
	ОбластьТовары = Макет.ПолучитьОбласть("Товары");
	ОбластьРасчетыШапка = Макет.ПолучитьОбласть("РасчетыШапка");
	ОбластьРасчеты = Макет.ПолучитьОбласть("Расчеты");
	ОбластьЦеныШапка = Макет.ПолучитьОбласть("ЦеныШапка");
	ОбластьЦены = Макет.ПолучитьОбласть("Цены");
	ОбластьОстаткиШапка = Макет.ПолучитьОбласть("ОстаткиШапка");
	ОбластьОстатки = Макет.ПолучитьОбласть("Остатки");
	ОбластьРасчетыСПоставщикамиШапка = Макет.ПолучитьОбласть("РасчетыСПоставщикамиШапка");
	ОбластьРасчетыСПоставщиками = Макет.ПолучитьОбласть("РасчетыСПоставщиками");
	Подвал = Макет.ПолучитьОбласть("Подвал");

	ТабДок.Очистить();
	
	валюта = Справочники.Валюты.ОсновнаяВалюта ;

	ВставлятьРазделительСтраниц = Ложь;
	Пока Выборка.Следующий() Цикл
		Если ВставлятьРазделительСтраниц Тогда
			ТабДок.ВывестиГоризонтальныйРазделительСтраниц();
		КонецЕсли;

		ТабДок.Вывести(ОбластьЗаголовок);

		Шапка.Параметры.Заполнить(Выборка);
		Шапка.Параметры.валюта = ?(ЗначениеЗаполнено(Выборка.ВидЦен), Выборка.ВидЦен.ВалютаЦены, "");
		ТабДок.Вывести(Шапка, Выборка.Уровень());

		ТабДок.Вывести(ОбластьТоварыШапка);
		ВыборкаТовары = Выборка.Товары.Выбрать();
		Пока ВыборкаТовары.Следующий() Цикл
			ОбластьТовары.Параметры.Заполнить(ВыборкаТовары);
			ТабДок.Вывести(ОбластьТовары, ВыборкаТовары.Уровень());
		КонецЦикла;

		ТабДок.Вывести(ОбластьРасчетыШапка);
		ВыборкаРасчеты = Выборка.Расчеты.Выбрать();
		Пока ВыборкаРасчеты.Следующий() Цикл
			ОбластьРасчеты.Параметры.Заполнить(ВыборкаРасчеты);
			ТабДок.Вывести(ОбластьРасчеты, ВыборкаРасчеты.Уровень());
		КонецЦикла;

		ТабДок.Вывести(ОбластьРасчетыСПоставщикамиШапка);
		ВыборкаРасчетыСПоставщиками = Выборка.РасчетыСПоставщиками.Выбрать();
		Пока ВыборкаРасчетыСПоставщиками.Следующий() Цикл
			ОбластьРасчетыСПоставщиками.Параметры.Заполнить(ВыборкаРасчетыСПоставщиками);
			ТабДок.Вывести(ОбластьРасчетыСПоставщиками, ВыборкаРасчетыСПоставщиками.Уровень());
		КонецЦикла;

		ТабДок.Вывести(ОбластьОстаткиШапка);
		ВыборкаОстатки = Выборка.ОстаткиДенег.Выбрать();
		Пока ВыборкаОстатки.Следующий() Цикл
			ОбластьОстатки.Параметры.Заполнить(ВыборкаОстатки);
			ТабДок.Вывести(ОбластьОстатки, ВыборкаОстатки.Уровень());
		КонецЦикла;

		ТабДок.Вывести(ОбластьЦеныШапка);
		ВыборкаЦены = Выборка.Цены.Выбрать();
		Пока ВыборкаЦены.Следующий() Цикл
			ОбластьЦены.Параметры.Заполнить(ВыборкаЦены);
			ОбластьЦены.Параметры.валюта = ?(ЗначениеЗаполнено(Выборка.ВидЦен), Выборка.ВидЦен.ВалютаЦены, валюта);
			ТабДок.Вывести(ОбластьЦены, ВыборкаЦены.Уровень());
		КонецЦикла;

		Подвал.Параметры.Заполнить(Выборка);
		Подвал.Параметры.валюта = валюта;
		ТабДок.Вывести(Подвал);

		ВставлятьРазделительСтраниц = Истина;
	КонецЦикла;
	
КонецПроцедуры

Процедура ОбработкаПолученияФормы(ВидФормы, Параметры, ВыбраннаяФорма, ДополнительнаяИнформация, СтандартнаяОбработка)
	
	ЗначенияЗаполнения = Неопределено;
	
	Если Параметры.свойство("ВзаимозачетДолгаКонтрагентов")
		или (Параметры.свойство("ЗначенияЗаполнения", ЗначенияЗаполнения)
		И ЗначенияЗаполнения.свойство("ВзаимозачетДолгаКонтрагентов")) Тогда
		
		СтандартнаяОбработка = Ложь;
		
		Если ВидФормы = "ФормаСписка" Тогда
			ВыбраннаяФорма = "ФормаСпискаДокументовВзаимозачетаДолгаКонтрагентов";
			
		ИначеЕсли ВидФормы = "ФормаОбъекта" Тогда
			ВыбраннаяФорма = "ФормаДокументаВзаимоЗачетаДолгаКонтрагентов";
			
		Иначе
			ВыбраннаяФорма = "ФормаВыбораДокументовВзаимозачетаДолгаКонтрагентов";
			
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры
