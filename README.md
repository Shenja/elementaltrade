
////////////////////////////////////////////////////////////////////////////////
// ПРОГРАММНЫЙ ИНТЕРФЕЙС



////////////////////////////////////////////////////////////////////////////////
// ИНТЕРФЕЙС ВНЕШНЕГО ОБЪЕКТА


Функция ПолучитьПустуюТаблицуРезультатов()
	Результат = Новый ТаблицаЗначений;
	Результат.Колонки.Вставить("Партнер", Новый ОписаниеТипов("СправочникСсылка.Партнеры"));
	Результат.Колонки.Вставить("Организация", Новый ОписаниеТипов("СправочникСсылка.Организации"));
	Результат.Колонки.Вставить("Подразделение", Новый ОписаниеТипов("СправочникСсылка.СтруктураПредприятия"));
	Результат.Колонки.Вставить("Сумма", Новый ОписаниеТипов("Число"));
	Результат.Колонки.Вставить("Расшфировка", Новый ОписаниеТипов("Строка"));
	
	Возврат Результат;
КонецФункции

// Основная функция по расчету показателей для заработной платы. 
//  Параметры:
//	ТаблицаОбъектов - ТаблицаЗначений
//		+ Партнет - СправочникСсылка.Партнеры 
//		+ Организация - СправочникСсылка.Организации
//		+ Подразделение - СправочникСсылка.Подаразделения
//		+ ВидНачисления
//	КомментироватьРасчет - Булево - По умолчанию Ложь, при истине будет записана расшифровка формирования суммы. 
//
//  Возвращаемое значение:
//	ТаблицаЗначение - содержит уже рассчитаные виды начислений 
//		+ Партнет - СправочникСсылка.Партнеры 
//		+ Организация - СправочникСсылка.Организации
//		+ Подразделение - СправочникСсылка.Подаразделения
//		+ ВидНачисления
//		+ Сумма - Число, 15.2
//		+ Расшифровка - Произвольное. 
Функция РасчитатьНачисления(ТаблицаОбъектов, КомментироватьРасчет) Экспорт 
	Перем ТаблицаРезультатов;
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	Запрос.Текст = "";
	
	ТаблицаРезультатов = ПолучитьПустуюТаблицуРезультатов();
	
	
	Возврат ТаблицаРезультатов;
КонецФункции //РасчитатьНачисления

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

// Получить значение дополнительного сведения документа заказ клента
// 
// Параметры:
//  Объект 	 - ДокументСсылка.ЗаказКлиента                               	  - Ссылка на документ заказ клиента
//	Свойство - ПланыВидовХарактеристикСсылка.ДополнительныеРеквизитыИСведения - Ссылка на дополнительное сведение
//
// Возвращаемое значение:
//   Произвольное - Значение дополнительного сведения
Функция ПолучитьЗначениеСведенияПоОбъектуИСвойству(Объект, Свойство) 
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	               |	ДополнительныеСведения.Значение
	               |ИЗ
	               |	РегистрСведений.ДополнительныеСведения КАК ДополнительныеСведения
	               |ГДЕ
	               |	ВЫРАЗИТЬ(ДополнительныеСведения.Объект КАК Документ.ЗаказКлиента) = &Объект
	               |И 	ДополнительныеСведения.Свойство = &Свойство";
	Запрос.УстановитьПараметр("Объект",   Объект);
	Запрос.УстановитьПараметр("Свойство", Свойство);
	Выборка = Запрос.Выполнить().Выбрать();
	Возврат ?(Выборка.Следующий(), Выборка.Значение, Неопределено); 
КонецФункции // ПолучитьЗначениеСведенияПоОбъектуИСвойству()



////////////////////////////////////////////////////////////////////////////////
// Информация о внешней обработке

Функция СведенияОВнешнейОбработке() Экспорт
	
	Версия = "1.0.29";
	
	// Объявим переменную, в которой мы сохраним и вернем "наружу" необходимые данные
    ПараметрыРегистрации = Новый Структура;

    // Объявим еще одну переменную, которая нам потребуется ниже
    МассивНазначений = Новый Массив;
    
    // Первый параметр, который мы должны указать - это какой вид обработки системе должна зарегистрировать. 
    // Допустимые типы: ДополнительнаяОбработка, ДополнительныйОтчет, ЗаполнениеОбъекта, Отчет, ПечатнаяФорма, СозданиеСвязанныхОбъектов
    ПараметрыРегистрации.Вставить("Вид", "ДополнительнаяОбработка");	
	ПараметрыРегистрации.Вставить("Назначение", Новый Массив);
    
    // Теперь зададим имя, под которым ВПФ будет зарегистрирована в справочнике внешних обработок
    ПараметрыРегистрации.Вставить("Наименование", "Расчитать начисления по ЗП");
    
    // Зададим право обработке на использование безопасного режима. Более подробно можно узнать в справке к платформе (метод УстановитьБезопасныйРежим)
    ПараметрыРегистрации.Вставить("БезопасныйРежим", Ложь);

    // Следующие два параметра играют больше информационную роль, т.е. это то, что будет видеть пользователь в информации к обработке
    ПараметрыРегистрации.Вставить("Версия", Версия);    
    ПараметрыРегистрации.Вставить("Информация", "Расчитать начисления по ЗП [" + Версия + "]");
    
    // Создадим таблицу команд (подробнее смотрим ниже)
    ТаблицаКоманд = ПолучитьТаблицуКоманд();
    
    // Добавим команду в таблицу
	ДобавитьКоманду(ТаблицаКоманд, "Расчитать начисления по ЗП [" + Версия + "]", "РасчитатьНачисления();", "ВызовСерверногоМетода");
		
    // Сохраним таблицу команд в параметры регистрации обработки
    ПараметрыРегистрации.Вставить("Команды", ТаблицаКоманд);
    
    // Теперь вернем системе наши параметры
    Возврат ПараметрыРегистрации;
	
КонецФункции

Функция ПолучитьТаблицуКоманд()

   // Создадим пустую таблицу команд и колонки в ней
   Команды = Новый ТаблицаЗначений;

   // Как будет выглядеть описание печатной формы для пользователя
   Команды.Колонки.Добавить("Представление", Новый ОписаниеТипов("Строка")); 

   // Имя нашего макета, что бы могли отличить вызванную команду в обработке печати
   Команды.Колонки.Добавить("Идентификатор", Новый ОписаниеТипов("Строка"));

   // Тут задается, как должна вызваться команда обработки
   // Возможные варианты:
   // - ОткрытиеФормы - в этом случае в колонке идентификатор должно быть указано имя формы, которое должна будет открыть система
   // - ВызовКлиентскогоМетода - вызвать клиентскую экспортную процедуру из модуля формы обработки
   // - ВызовСерверногоМетода - вызвать серверную экспортную процедуру из модуля объекта обработки
   Команды.Колонки.Добавить("Использование", Новый ОписаниеТипов("Строка"));

   // Следующий параметр указывает, необходимо ли показывать оповещение при начале и завершению работы обработки. Не имеет смысла при открытии формы
   Команды.Колонки.Добавить("ПоказыватьОповещение", Новый ОписаниеТипов("Булево"));

   // Для печатной формы должен содержать строку ПечатьMXL 
   Команды.Колонки.Добавить("Модификатор", Новый ОписаниеТипов("Строка"));
   
   Возврат Команды;
   
КонецФункции

Процедура ДобавитьКоманду(ТаблицаКоманд, Представление, Идентификатор, Использование, ПоказыватьОповещение = Ложь, Модификатор = "")
  // Добавляем команду в таблицу команд по переданному описанию.
  // Параметры и их значения можно посмотреть в функции ПолучитьТаблицуКоманд
  НоваяКоманда = ТаблицаКоманд.Добавить();
  НоваяКоманда.Представление = Представление;
  НоваяКоманда.Идентификатор = Идентификатор;
  НоваяКоманда.Использование = Использование;
  НоваяКоманда.ПоказыватьОповещение = ПоказыватьОповещение;
  НоваяКоманда.Модификатор = Модификатор;

КонецПроцедуры

Процедура ВыполнитьКоманду(Индентификатор) Экспорт
	Выполнить(Индентификатор);	
КонецПроцедуры



////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ ПЕРЕОПРЕДЕЛЕНИЯ ОБРАБОТЧИКОВ


